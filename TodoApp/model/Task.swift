//
//  Task.swift
//  TodoApp
//
//  Created by cice on 19/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import Foundation


enum TaskType : String, CaseIterable{
    case house
    case work
    case family
    case friends
    case undefined
}

enum Priority: String, CaseIterable{
    case high
    case normal
    case low
}

class Task {
    var name:String
    var type: TaskType
    var priority : Priority
    
    init(name: String, type:TaskType, priority:Priority) {
        self.name=name
        self.priority=priority
        self.type=type
    }
}
