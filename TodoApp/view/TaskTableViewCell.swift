//
//  TaskTableViewCell.swift
//  TodoApp
//
//  Created by cice on 19/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit



class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var priorityLB: UILabel!
    @IBOutlet weak var typeLB: UILabel!
    @IBOutlet weak var nameLB: UILabel!
      
    func configureCell(task: Task){        
        nameLB.text = task.name
        typeLB.text = task.type.rawValue
        priorityLB.text = task.priority.rawValue
    }
}
