//
//  TaskListViewController.swift
//  TodoApp
//
//  Created by cice on 19/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

class TaskListViewController: UIViewController {
    
    var taskList = [Task]()
    @IBOutlet weak var taskTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        taskTableView.register((UINib(nibName: "TaskTableViewCell", bundle: nil)), forCellReuseIdentifier: "idView")
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IdSegueCreateTask" {
            let task = Task(name: "algo", type: .undefined, priority: .normal)
            taskList.append(task)
            
            let editTaskViewController = segue.destination as? EditTaskViewController
            editTaskViewController?.delegate = self
            editTaskViewController?.task = task
            taskTableView.reloadData()
        }
    }
}


extension TaskListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "idView", for: indexPath) as? TaskTableViewCell
        cell?.configureCell(task: taskList[indexPath.row])
        
        return cell ?? UITableViewCell()
    }   
    
}

extension TaskListViewController: EditTaskViewControllerDelegate{
    func editFinished() {
         taskTableView.reloadData()
         dismiss(animated: true, completion: nil)
    }
    
}
