//
//  EditTaskViewController.swift
//  TodoApp
//
//  Created by cice on 19/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

protocol EditTaskViewControllerDelegate {
    
    func editFinished()
}

class EditTaskViewController: UIViewController {

    @IBOutlet weak var nameTxt: UITextField!
    var delegate: EditTaskViewControllerDelegate?
    var task: Task?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func okButtonAction(_ sender: UIButton) {
        delegate?.editFinished()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameTxt.text = task?.name        
    }
    
}
